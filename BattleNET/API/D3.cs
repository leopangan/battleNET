﻿using BattleNET.Objects.D3;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.API
{
    public class D3
    {
        public BattleNETClient parent { get; internal set; }

        private string api_url;
        private string locale;
        private string api_key;
        private string user_agent;

        public D3(BattleNETClient parent, string api_url, string locale, string api_key, string user_agent)
        {
            this.parent = parent;

            this.api_url = api_url;
            this.locale = locale;
            this.api_key = api_key;
            this.user_agent = user_agent;
        }

        #region PROFILE API
        public CareerProfile getCareerProfile(string battletag)
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}d3/profile/{battletag}/?locale={locale}&apikey={api_key}");

            return new CareerProfile(JObject.Parse(request.response));
        }
        public Heroe getHeroProfile(string battletag, int id)
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}d3/profile/{battletag}/hero/{id}?locale={locale}&apikey={api_key}");

            return new Heroe(JObject.Parse(request.response));
        }
        #endregion

        #region DATA RESOURCES
        public Item getItemData(string data)
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}d3/data/item/{data}?locale={locale}&apikey={api_key}");

            return new Item(JObject.Parse(request.response));
        }
        public enum follower
        {
            enchantress,
            scoundrel,
            templar
        }
        public Heroe getFollowerData(follower follower)
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}d3/data/follower/{follower}?locale={locale}&apikey={api_key}");

            return new Heroe(JObject.Parse(request.response));
        }
        public enum artisan
        {
            blacksmith,
            jeweler,
            mystic
        }
        public Artisan getArtisanData(artisan artisan)
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}d3/data/artisan/{artisan}?locale={locale}&apikey={api_key}");

            return new Artisan(JObject.Parse(request.response));
        }
        #endregion

        #region getIcons
        public enum ItemImageSize
        {
            small,
            large
        }
        public Image getItemImage(ItemImageSize size, string icon)
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create($"http://media.blizzard.com/d3/icons/items/{size}/{icon}.png");
            HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            Stream stream = httpWebResponse.GetResponseStream();

            return Image.FromStream(stream);
        }
        public enum SkillImageSize
        {
            x21,
            x42,
            x64
        }
        public Image getSkillImage(SkillImageSize size, string icon)
        {
            int tSize;
            switch (size)
            {
                case SkillImageSize.x21:
                    tSize = 21;
                    break;
                case SkillImageSize.x42:
                    tSize = 42;
                    break;
                case SkillImageSize.x64:
                    tSize = 64;
                    break;
                default:
                    tSize = 64;
                    break;
            }

            HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create($"http://media.blizzard.com/d3/icons/skills/{tSize}/{icon}.png");
            HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            Stream stream = httpWebResponse.GetResponseStream();

            return Image.FromStream(stream);
        }
        #endregion

    }
}
