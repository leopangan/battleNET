﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET
{
    public class BattleNETClient
    {
        public event EventHandler<ConnectedEventArgs> Connected;

        protected virtual void OnConnected(ConnectedEventArgs e)
        {
            EventHandler<ConnectedEventArgs> handler = Connected;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public enum BattleNetRegion
        {
            US,
            EU,
            KR,
            TW
        }

        public enum BattleNetLocale
        {
            en_GB,
            de_DE,
            es_ES,
            fr_FR,
            it_IT,
            pl_PL,
            pt_PT,
            ru_RU,
            ko_KR,
            zh_TW,
            en_US,
            pt_BR,
            es_MX
        }

        private Dictionary<string, List<string>> connectiontest = new Dictionary<string, List<string>>();

        private string OAUTH_URL    = "https://{0}.battle.net/";
        private string API_URL = "https://{0}.api.battle.net/";
        const string USER_AGENT = "";

        private string api_key;
        private string api_secret;
        private BattleNetRegion api_region;
        private BattleNetLocale api_locale;
        
        public API.WOW WOW { get; internal set; }
        public API.D3 D3 { get; internal set; }
        //public API.OAuth OAuth { get; internal set; }

        public BattleNETClient(string key, string secret, BattleNetRegion region, BattleNetLocale locale)
        {
            this.api_key = key;
            this.api_secret = secret;
            this.api_region = region;
            this.api_locale = locale;

            this.OAUTH_URL = string.Format(this.OAUTH_URL, this.api_region);
            this.API_URL = string.Format(this.API_URL, this.api_region);
            this.WOW = new API.WOW(this, API_URL, api_locale.ToString(), api_key, USER_AGENT);
            this.D3 = new API.D3(this, API_URL, api_locale.ToString(), api_key, USER_AGENT);
            //this.OAuth = new API.OAuth(this, OAUTH_URL, api_locale.ToString(), api_key, api_secret, USER_AGENT);

            List<string> connectionstesturls = new List<string>();
            connectionstesturls.Add($"{this.API_URL}wow/achievement/2144?locale={this.api_locale}&apikey={this.api_key}");
            connectionstesturls.Add($"{this.API_URL}wow/boss/24723?locale={this.api_locale}&apikey={this.api_key}");
            connectionstesturls.Add($"{this.API_URL}wow/item/18803?locale={this.api_locale}&apikey={this.api_key}");
            connectionstesturls.Add($"{this.API_URL}wow/item/set/1060?locale={this.api_locale}&apikey={this.api_key}");
            connectionstesturls.Add($"{this.API_URL}wow/pet/ability/640?locale={this.api_locale}&apikey={this.api_key}");
            connectionstesturls.Add($"{this.API_URL}wow/pet/species/258?locale={this.api_locale}&apikey={this.api_key}");
            connectionstesturls.Add($"{this.API_URL}wow/pet/stats/258?locale={this.api_locale}&apikey={this.api_key}");
            connectionstesturls.Add($"{this.API_URL}wow/quest/13146?locale={this.api_locale}&apikey={this.api_key}");
            connectionstesturls.Add($"{this.API_URL}wow/recipe/33994?locale={this.api_locale}&apikey={this.api_key}");
            connectionstesturls.Add($"{this.API_URL}wow/spell/8056?locale={this.api_locale}&apikey={this.api_key}");
            connectionstesturls.Add($"{this.API_URL}wow/zone/4131?locale={this.api_locale}&apikey={this.api_key}");
            this.connectiontest.Add("WoW-API", connectionstesturls);

            connectionstesturls = new List<string>();

        }

        public BattleNetRegion getAPIRegion()
        {
            return api_region;
        }

        public BattleNetLocale getApiLocale()
        {
            return api_locale;
        }

        public void CredentialCheck()
        {
            Task task = new Task(CredentialCheckAsync);
            task.Start();
            task.Wait();
        }

        public async void CredentialCheckAsync()
        {
            List<string> connectionstesturls = new List<string>();
            connectionstesturls.Add($"{this.API_URL}wow/achievement/2144?locale={this.api_locale}&apikey={this.api_key}");
            connectionstesturls.Add($"{this.API_URL}wow/boss/24723?locale={this.api_locale}&apikey={this.api_key}");
            connectionstesturls.Add($"{this.API_URL}wow/item/18803?locale={this.api_locale}&apikey={this.api_key}");
            connectionstesturls.Add($"{this.API_URL}wow/item/set/1060?locale={this.api_locale}&apikey={this.api_key}");
            connectionstesturls.Add($"{this.API_URL}wow/pet/ability/640?locale={this.api_locale}&apikey={this.api_key}");
            connectionstesturls.Add($"{this.API_URL}wow/pet/species/258?locale={this.api_locale}&apikey={this.api_key}");
            connectionstesturls.Add($"{this.API_URL}wow/pet/stats/258?locale={this.api_locale}&apikey={this.api_key}");
            connectionstesturls.Add($"{this.API_URL}wow/quest/13146?locale={this.api_locale}&apikey={this.api_key}");
            connectionstesturls.Add($"{this.API_URL}wow/recipe/33994?locale={this.api_locale}&apikey={this.api_key}");
            connectionstesturls.Add($"{this.API_URL}wow/spell/8056?locale={this.api_locale}&apikey={this.api_key}");
            connectionstesturls.Add($"{this.API_URL}wow/zone/4131?locale={this.api_locale}&apikey={this.api_key}");
            Random rnd = new Random();

            string url = connectionstesturls[rnd.Next(connectionstesturls.Count())];

            Request request = new Request(USER_AGENT);
            Task<int> status = request.GETStatusAsync(url);

            int x = await status;
            if (x.ToString().StartsWith("2"))
            {
                ConnectedEventArgs e = new ConnectedEventArgs();
                e.key = this.api_key;
                e.locale = this.api_locale;
                e.region = this.api_region;
                e.status = x;
                e.url = url;
                OnConnected(e);
            }
        }
    }

    public class ConnectedEventArgs : EventArgs
    {
        public string key { get; internal set; }
        public BattleNETClient.BattleNetRegion region { get; internal set; }
        public BattleNETClient.BattleNetLocale locale { get; internal set; }
        public int status { get; internal set; }
        public string url { get; internal set; }
    }
}
