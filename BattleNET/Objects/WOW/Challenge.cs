﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.WOW
{
    public class Challenge
    {
        public class ChallengeRealm
        {
            public string name { get; internal set; }
            public string slug { get; internal set; }
            public string battlegroup { get; internal set; }
            public string locale { get; internal set; }
            public string timezone { get; internal set; }
            public List<string> connected_realms { get; internal set; }

            public ChallengeRealm(JObject JRealm)
            {
                this.name = JRealm["name"].ToString();
                this.slug = JRealm["slug"].ToString();
                this.battlegroup = JRealm["battlegroup"].ToString();
                this.locale = JRealm["locale"].ToString();
                this.timezone = JRealm["timezone"].ToString();
                this.connected_realms = new List<string>();
                foreach (string JConnected_Realms in JRealm["connected_realms"])
                    this.connected_realms.Add(JConnected_Realms);
            }
        }
        public class ChallengeMap
        {
            public class ChallengeMapCriteria
            {
                public long time { get; internal set; }
                public int hours { get; internal set; }
                public int minutes { get; internal set; }
                public int seconds { get; internal set; }
                public int milliseconds { get; internal set; }
                public bool isPositive { get; internal set; }

                public ChallengeMapCriteria(JObject JMapCriteria)
                {
                    this.time = long.Parse(JMapCriteria["time"].ToString());
                    this.hours = int.Parse(JMapCriteria["hours"].ToString());
                    this.minutes = int.Parse(JMapCriteria["minutes"].ToString());
                    this.seconds = int.Parse(JMapCriteria["seconds"].ToString());
                    this.milliseconds = int.Parse(JMapCriteria["milliseconds"].ToString());
                    this.isPositive = bool.Parse(JMapCriteria["isPositive"].ToString());
                }
            }
            public int id { get; internal set; }
            public string name { get; internal set; }
            public string slug { get; internal set; }
            public bool hasChallengeMode { get; internal set; }
            public ChallengeMapCriteria bronzeCriteria { get; internal set; }
            public ChallengeMapCriteria silverCriteria { get; internal set; }
            public ChallengeMapCriteria goldCriteria { get; internal set; }

            public ChallengeMap(JObject JMap)
            {
                this.id = int.Parse(JMap["id"].ToString());
                this.name = JMap["name"].ToString();
                this.slug = JMap["slug"].ToString();
                this.hasChallengeMode = bool.Parse(JMap["hasChallengeMode"].ToString());
                this.bronzeCriteria = new ChallengeMapCriteria(JObject.Parse(JMap["bronzeCriteria"].ToString()));
                this.silverCriteria = new ChallengeMapCriteria(JObject.Parse(JMap["silverCriteria"].ToString()));
                this.goldCriteria = new ChallengeMapCriteria(JObject.Parse(JMap["goldCriteria"].ToString()));
            }
        }
        public class ChallengeGroup
        {
            public class ChallengeGroupTime
            {
                public long time { get; internal set; }
                public int hours { get; internal set; }
                public int minutes { get; internal set; }
                public int seconds { get; internal set; }
                public int milliseconds { get; internal set; }
                public bool isPositive { get; internal set; }

                public ChallengeGroupTime(JObject JGroupTime)
                {
                    this.time = long.Parse(JGroupTime["time"].ToString());
                    this.hours = int.Parse(JGroupTime["hours"].ToString());
                    this.minutes = int.Parse(JGroupTime["minutes"].ToString());
                    this.seconds = int.Parse(JGroupTime["seconds"].ToString());
                    this.milliseconds = int.Parse(JGroupTime["milliseconds"].ToString());
                    this.isPositive = bool.Parse(JGroupTime["isPositive"].ToString());
                }
            }

            public class ChallengeGroupMember
            {
                public Character character { get; internal set; }
                public Character.CharacterSpec spec { get; internal set; }

                public ChallengeGroupMember(JObject JGroupMember)
                {
                    if (JGroupMember["character"] != null && JGroupMember["character"].HasValues)
                        this.character = new Character(JObject.Parse(JGroupMember["character"].ToString()));
                    if (JGroupMember["spec"] != null && JGroupMember["spec"].HasValues)
                        this.spec = new Character.CharacterSpec(JObject.Parse(JGroupMember["spec"].ToString()));
                }
            }

            public int ranking { get; internal set; }
            public ChallengeGroupTime time { get; internal set; }
            public string date { get; internal set; }
            public string medal { get; internal set; }
            public string faction { get; internal set; }
            public bool isRecurring { get; internal set; }
            public List<ChallengeGroupMember> members { get; internal set; }
            public Guild guild { get; internal set; }

            public ChallengeGroup(JObject JGroup)
            {
                this.ranking = int.Parse(JGroup["ranking"].ToString());
                this.time = new ChallengeGroupTime(JObject.Parse(JGroup["time"].ToString()));
                this.date = JGroup["date"].ToString();
                this.medal = JGroup["medal"].ToString();
                this.faction = JGroup["faction"].ToString();
                this.isRecurring = bool.Parse(JGroup["isRecurring"].ToString());
                if (JGroup["members"] != null && JGroup["members"].HasValues)
                {
                    this.members = new List<ChallengeGroupMember>();
                    foreach (JObject JChallengeGroupMember in JGroup["members"])
                        this.members.Add(new ChallengeGroupMember(JChallengeGroupMember));
                }
                if (JGroup["guild"] != null && JGroup["guild"].HasValues)
                    this.guild = new Guild(JObject.Parse(JGroup["guild"].ToString()));
            }
        }

        public ChallengeRealm realm { get; internal set; }
        public ChallengeMap map { get; internal set; }
        public List<ChallengeGroup> groups { get; internal set; }

        public Challenge(JObject rawJson)
        {
            this.realm = new ChallengeRealm(JObject.Parse(rawJson["realm"].ToString()));
            this.map = new ChallengeMap(JObject.Parse(rawJson["map"].ToString()));
            this.groups = new List<ChallengeGroup>();
            foreach (JObject JGroup in rawJson["groups"])
                this.groups.Add(new ChallengeGroup(JGroup));
        }
    }
}
