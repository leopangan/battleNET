﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.WOW
{
    public class Recipe
    {
        public int id { get; internal set; }
        public string name { get; internal set; }
        public string profession { get; internal set; }
        public string icon { get; internal set; }

        public Recipe(JObject rawJson)
        {
            this.id = int.Parse(rawJson["id"].ToString());
            this.name = rawJson["name"].ToString();
            this.profession = rawJson["profession"].ToString();
            this.icon = rawJson["icon"].ToString();
        }
    }
}
