﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.D3
{
    public class CareerProfile
    {
        public class CareerProfileKills
        {
            public int monsters { get; internal set; }
            public int elites { get; internal set; }
            public int hardcoreMonsters { get; internal set; }

            public CareerProfileKills(JObject JKills)
            {
                this.monsters = int.Parse(JKills["monsters"].ToString());
                this.elites = int.Parse(JKills["elites"].ToString());
                this.hardcoreMonsters = int.Parse(JKills["hardcoreMonsters"].ToString());
            }
        }
        public class CareerProfileTimePlayed
        {
            public float barbarian { get; internal set; }
            public float crusader { get; internal set; }
            [JsonProperty("demon-hunter")]
            public float demonHunter { get; internal set; }
            public float monk { get; internal set; }
            [JsonProperty("witch-doctor")]
            public float witchDoctor { get; internal set; }
            public float wizard { get; internal set; }

            public CareerProfileTimePlayed(JObject JTimePlayed)
            {
                this.barbarian = float.Parse(JTimePlayed["barbarian"].ToString());
                this.crusader = float.Parse(JTimePlayed["crusader"].ToString());
                this.demonHunter = float.Parse(JTimePlayed["demon-hunter"].ToString());
                this.monk = float.Parse(JTimePlayed["monk"].ToString());
                this.witchDoctor = float.Parse(JTimePlayed["witch-doctor"].ToString());
                this.wizard = float.Parse(JTimePlayed["wizard"].ToString());
            }
        }
        public class CareerProfileProgression
        {
            public bool act1 { get; internal set; }
            public bool act2 { get; internal set; }
            public bool act3 { get; internal set; }
            public bool act4 { get; internal set; }
            public bool act5 { get; internal set; }
            
            public CareerProfileProgression(JObject JProgression)
            {
                this.act1 = bool.Parse(JProgression["act1"].ToString());
                this.act2 = bool.Parse(JProgression["act2"].ToString());
                this.act3 = bool.Parse(JProgression["act3"].ToString());
                this.act4 = bool.Parse(JProgression["act4"].ToString());
                this.act5 = bool.Parse(JProgression["act5"].ToString());
            }
        }
        public class CareerProfileSkill
        {
            public string slug { get; internal set; }
            public int level { get; internal set; }
            public int stepCurrent { get; internal set; }
            public int stepMax { get; internal set; }

            public CareerProfileSkill(JObject JSkill)
            {
                this.slug = JSkill["slug"].ToString();
                this.level = int.Parse(JSkill["level"].ToString());
                this.stepCurrent = int.Parse(JSkill["stepCurrent"].ToString());
                this.stepMax = int.Parse(JSkill["stepMax"].ToString());
            }
        }
        public class CareerProfileSeasonalProfiles
        {
            public CareerProfile season0 { get; internal set; }
            public CareerProfile season1 { get; internal set; }
            public CareerProfile season2 { get; internal set; }
            public CareerProfile season3 { get; internal set; }
            public CareerProfile season4 { get; internal set; }
            public CareerProfile season5 { get; internal set; }

            public CareerProfileSeasonalProfiles(JObject JSeasonalProfiles)
            {
                this.season0 = new CareerProfile(JObject.Parse(JSeasonalProfiles["season0"].ToString()));
                this.season1 = new CareerProfile(JObject.Parse(JSeasonalProfiles["season1"].ToString()));
                this.season2 = new CareerProfile(JObject.Parse(JSeasonalProfiles["season2"].ToString()));
                this.season3 = new CareerProfile(JObject.Parse(JSeasonalProfiles["season3"].ToString()));
                this.season4 = new CareerProfile(JObject.Parse(JSeasonalProfiles["season4"].ToString()));
                this.season5 = new CareerProfile(JObject.Parse(JSeasonalProfiles["season5"].ToString()));
            }
        }
        public string battleTag { get; internal set; }
        public int paragonLevel { get; internal set; }
        public int paragonLevelHardcore { get; internal set; }
        public int paragonLevelSeason { get; internal set; }
        public int paragonLevelSeasonHardcore { get; internal set; }
        public string guildName { get; internal set; }
        public List<Heroe> heroes { get; internal set; }
        public int lastPlayedHeroe { get; internal set; }
        public long lastUpdated { get; internal set; }
        public CareerProfileKills kills { get; internal set; }
        public int highestHardcoreLevel { get; internal set; }
        public CareerProfileTimePlayed timePlayed { get; internal set; }
        public CareerProfileProgression progression { get; internal set; }
        public List<Heroe> fallenHeroes { get; internal set; }
        public CareerProfileSkill blacksmith { get; internal set; }
        public CareerProfileSkill jeweler { get; internal set; }
        public CareerProfileSkill mystic { get; internal set; }
        public CareerProfileSkill blacksmithHardcore { get; internal set; }
        public CareerProfileSkill jewelerHardcore { get; internal set; }
        public CareerProfileSkill mysticHardcore { get; internal set; }
        public CareerProfileSkill blacksmithSeason { get; internal set; }
        public CareerProfileSkill jewelerSeason { get; internal set; }
        public CareerProfileSkill mysticSeason { get; internal set; }
        public CareerProfileSeasonalProfiles seasonalProfiles { get; internal set; }

        public CareerProfile(JObject rawJson)
        {
            this.battleTag = rawJson["battleTag"].ToString();
            this.paragonLevel = int.Parse(rawJson["paragonLevel"].ToString());
            this.paragonLevelHardcore = int.Parse(rawJson["paragonLevelHardcore"].ToString());
            this.paragonLevelSeason = int.Parse(rawJson["paragonLevelSeason"].ToString());
            this.paragonLevelSeasonHardcore = int.Parse(rawJson["paragonLevelSeasonHardcore"].ToString());
            this.guildName = rawJson["guildName"].ToString();
            this.heroes = new List<Heroe>();
            foreach (JObject JHeroe in rawJson["heroes"])
                this.heroes.Add(new Heroe(JHeroe));
            this.lastPlayedHeroe = int.Parse(rawJson["lastPlayedHeroe"].ToString());
            this.lastUpdated = long.Parse(rawJson["lastUpdated"].ToString());
            this.kills = new CareerProfileKills(JObject.Parse(rawJson["kills"].ToString()));
            this.highestHardcoreLevel = int.Parse(rawJson["highestHardcoreLevel"].ToString());
            this.timePlayed = new CareerProfileTimePlayed(JObject.Parse(rawJson["timePlayed"].ToString()));
            this.progression = new CareerProfileProgression(JObject.Parse(rawJson["progression"].ToString()));
            this.fallenHeroes = new List<Heroe>();
            foreach (JObject JFallenHeroe in rawJson["fallenHeroes"])
                this.fallenHeroes.Add(new Heroe(JFallenHeroe));
            this.blacksmith = new CareerProfileSkill(JObject.Parse(rawJson["blacksmith"].ToString()));
            this.jeweler = new CareerProfileSkill(JObject.Parse(rawJson["jeweler"].ToString()));
            this.mystic = new CareerProfileSkill(JObject.Parse(rawJson["mystic"].ToString()));
            this.blacksmithHardcore = new CareerProfileSkill(JObject.Parse(rawJson["blacksmithHardcore"].ToString()));
            this.jewelerHardcore = new CareerProfileSkill(JObject.Parse(rawJson["jewelerHardcore"].ToString()));
            this.mysticHardcore = new CareerProfileSkill(JObject.Parse(rawJson["mysticHardcore"].ToString()));
            this.blacksmithSeason = new CareerProfileSkill(JObject.Parse(rawJson["blacksmithSeason"].ToString()));
            this.jewelerSeason = new CareerProfileSkill(JObject.Parse(rawJson["jewelerSeason"].ToString()));
            this.mysticSeason = new CareerProfileSkill(JObject.Parse(rawJson["mysticSeason"].ToString()));
            this.seasonalProfiles = new CareerProfileSeasonalProfiles(JObject.Parse(rawJson["seasonalProfiles"].ToString()));
        }
    }
}
