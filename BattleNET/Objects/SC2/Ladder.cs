﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.SC2
{
    public class Ladder
    {
        public class LadderLadderMember
        {
            public Profile character { get; internal set; }
            public long joinTimestamp { get; internal set; }
            public double points { get; internal set; }
            public int wins { get; internal set; }
            public int losses { get; internal set; }
            public int highestRank { get; internal set; }
            public int previousRank { get; internal set; }
            public string favoriteRaceP1 { get; internal set; }

            public LadderLadderMember(JObject JLadderMember)
            {
                this.character = new Profile(JObject.Parse(JLadderMember["character"].ToString()));
                this.joinTimestamp = long.Parse(JLadderMember["joinTimestamp"].ToString());
                this.points = double.Parse(JLadderMember["points"].ToString());
                this.wins = int.Parse(JLadderMember["wins"].ToString());
                this.losses = int.Parse(JLadderMember["losses"].ToString());
                this.highestRank = int.Parse(JLadderMember["highestRank"].ToString());
                this.previousRank = int.Parse(JLadderMember["previousRank"].ToString());
                this.favoriteRaceP1 = JLadderMember["favoriteRaceP1"].ToString();
            }
        }

        public List<LadderLadderMember> ladderMembers { get; internal set; }

        public Ladder(JObject rawJson)
        {
            this.ladderMembers = new List<LadderLadderMember>();

            foreach (JObject JLadderMember in rawJson["ladderMembers"])
                this.ladderMembers.Add(new LadderLadderMember(JLadderMember));
        }
    }
}
