﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.SC2
{
    public class Match
    {
        public string map { get; internal set; }
        public string type { get; internal set; }
        public string decision { get; internal set; }
        public string speed { get; internal set; }
        public long date { get; internal set; }

        public Match(JObject rawJson)
        {
            this.map = rawJson["map"].ToString();
            this.type = rawJson["type"].ToString();
            this.decision = rawJson["decision"].ToString();
            this.speed = rawJson["speed"].ToString();
            this.date = long.Parse(rawJson["date"].ToString());
        }
    }
}
