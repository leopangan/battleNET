﻿using BattleNET;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET_Console
{
    class Program
    {

        public static void Main(string[] args)
        {
            StreamReader sr = new StreamReader("credentials.txt");
            BattleNETClient client = new BattleNETClient(sr.ReadLine(), sr.ReadLine(), BattleNETClient.BattleNetRegion.EU, BattleNETClient.BattleNetLocale.de_DE);
            sr.Close();

            client.Connected += (sender, e) =>
            {
                Console.WriteLine($"Connected");

                BattleNET.Objects.WOW.Achievement achievement = client.WOW.getAchievement(2144);
                Console.WriteLine($"{achievement.id}: {achievement.title}");
            };

            client.CredentialCheck();

            /*using (StreamWriter sw = new StreamWriter("achievement.txt", false))
            {
                BattleNET.Objects.WOW.Achievement achievement = client.WOW.getAchievement(2144);

                sw.WriteLine(JsonConvert.SerializeObject(achievement));
                sw.WriteLine();
                sw.WriteLine($"{achievement.id}: {achievement.title}");
                sw.WriteLine($"{achievement.description}");
                sw.Flush();
                sw.Close();
            }
            using (StreamWriter sw = new StreamWriter("auction.txt", false))
            {
                List<BattleNET.Objects.WOW.Auction> auctions = client.WOW.getAuctions("blackmoore");

                sw.WriteLine(JsonConvert.SerializeObject(auctions));
                sw.WriteLine();
                sw.WriteLine($"{auctions[0].owner}-{auctions[0].ownerRealm}: {auctions[0].item}");
                sw.Flush();
                sw.Close();
            }
            using (StreamWriter sw = new StreamWriter("boss.txt", false))
            {
                BattleNET.Objects.WOW.Boss boss = client.WOW.getBoss(24723);

                sw.WriteLine(JsonConvert.SerializeObject(boss));
                sw.WriteLine();
                sw.WriteLine($"{boss.id}: {boss.name}");
                sw.WriteLine($"{boss.description}");
                sw.Flush();
                sw.Close();
            }
            using (StreamWriter sw = new StreamWriter("challenge.txt", false))
            {
                List<BattleNET.Objects.WOW.Challenge> challenges = client.WOW.getRealmLeaderboards("blackmoore");

                sw.WriteLine(JsonConvert.SerializeObject(challenges));
                sw.WriteLine();
                sw.WriteLine($"{challenges[0].groups[0].guild}: {challenges[0].groups[0].date}");
                sw.Flush();
                sw.Close();
            }
            using (StreamWriter sw = new StreamWriter("character.txt", false))
            {
                BattleNET.Objects.WOW.Character character = client.WOW.getCharacterProfile("blackmoore", "Shypriest", "achievements,appearance,feed");

                sw.WriteLine(JsonConvert.SerializeObject(character));
                sw.WriteLine();
                sw.WriteLine($"{character.name}: {character.level}");
                sw.WriteLine($"Honorable Kills: {character.totalHonorableKills}");
                sw.Flush();
                sw.Close();
            }
            using (StreamWriter sw = new StreamWriter("guild.txt", false))
            {
                BattleNET.Objects.WOW.Guild guild = client.WOW.getGuildProfile("blackmoore", "addicted", "members,achievements,news,challenge");

                sw.WriteLine(JsonConvert.SerializeObject(guild.challenge));
                sw.WriteLine();
                sw.WriteLine($"{guild.name}: {guild.level}");
                sw.WriteLine($"{guild.battlegroup}");
                sw.Flush();
                sw.Close();
            }
            using (StreamWriter sw = new StreamWriter("pet.txt", false))
            {
                BattleNET.Objects.WOW.Pet.PetSpecies pet = client.WOW.getPetSpecies(258);

                sw.WriteLine(JsonConvert.SerializeObject(pet));
                sw.WriteLine();
                sw.WriteLine($"{pet.name}: {pet.speciesId}");
                sw.WriteLine($"{pet.description}");
                sw.WriteLine();
                sw.WriteLine($"[{pet.abilities[0].requiredLevel}] {pet.abilities[0].name}");
                sw.Flush();
                sw.Close();
            }*/
            do
            {
                Console.ReadLine();
            } while (true);
        }
    }
}
